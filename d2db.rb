require 'sinatra'
require 'sinatra/namespace'
require 'json'

require_relative 'db'

db = DB.new 'db.sqlite'

set :bind, '0.0.0.0'
set :port, 6100

get '/' do
    haml :index
end

post '/import_keys' do
    data = File.open params[:keys][:tempfile].path

    added = data.each_line.map do |line|
        r = line.match /Classic=([\w\d]+).*Expansion=([\w\d]+).*Comment=([\w\d]*)/
        r = line.match /^([\w\d]+):([\w\d]+):.*?:.*?:?([\w\d]*)$/ unless r

        begin
            db.add_key(*r[1..3]) if r
            r ? :ok : :nomatch
        rescue
            :error
        end
    end
        .group_by{ |x| x }
        .map{ |k,v| [k, v.size] }
        .to_h

    haml :import_keys, locals: {added: added}
end

get '/keys' do
    haml :keys, locals: { keys: db.get_keys }
end

get '/proxies' do
    p db.get_proxies
    haml :proxies, locals: { proxies: db.get_proxies }
end

post '/delete_proxy/:id' do |id|

end

post '/import_proxies' do
    data = File.open params[:proxies][:tempfile].path

    added = data.each_line.map do |line|
        r = line.match /^([\w\d]+):(\d+):?([\w\d]*):?([\w\d]*)$/

        # puts '%s %s %s %s' % r[1..4]

        begin
            p r
            db.add_proxy(*r[1..4]) if r
            r ? :ok : :nomatch
        rescue
            :error
        end
    end
        .group_by{ |x| x }
        .map{ |k,v| [k, v.size] }
        .to_h

    haml :import_keys, locals: {added: added}
end

post '/execute_sql' do 
    db.execute params[:sql]
    redirect url('/'), 303
end

namespace '/api/v1' do

    before do
        content_type 'application/json'
    end

    namespace '/key' do

        get '/get' do
            k = db.allocate_key <<-SQL
            SELECT * FROM keys
            WHERE banned != 1 AND in_use = 0
            ORDER BY last_used ASC, id ASC
            LIMIT 1
            SQL
            k.to_json
        end

        get '/free/:id' do |id|
            db.free_key id
            {}.to_json
        end

        post '/update/:id' do |id|
            p params
            qry = <<-SQL
            UPDATE keys
            SET %{updates}
            WHERE id = :id
            SQL

            qry = qry % {
                updates: params
                    .reject{ |k, _| k == 'id' }
                    .map{ |k, _| "#{k} = :#{k}" }
                    .join(', ')
            }

            puts qry

            db.update_key qry, params
            {}.to_json
        end

    end

    namespace '/proxy' do

        get '/get' do
            k = db.allocate_proxy <<-SQL
            SELECT * FROM proxies
            WHERE in_use = 0
            ORDER BY id ASC
            LIMIT 1
            SQL
            k.to_json
        end

        get '/count' do
            r = db.execute <<-SQL
            SELECT COUNT(*) FROM proxies
            WHERE do_not_use = 0
            SQL
            { count: r[0][0].to_i }.to_json
        end

        get '/free/:id' do |id|
            db.free_proxy id
            {}.to_json
        end
    end
    
end