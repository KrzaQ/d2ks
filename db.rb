require 'sqlite3'
require 'faker'

class DB

    def initialize(dbname)
        @db = SQLite3::Database.new dbname
        # @db.type_translation = true
        @db.results_as_hash = true

        create_tables

    end

    def allocate_key(qry, params = {})
        @db.transaction :exclusive

        result = @db.execute qry, params
        
        if result.size == 0
            @db.rollback
            return nil
        end

        @db.execute 'UPDATE keys SET in_use = 1, last_used = CURRENT_TIMESTAMP ' +
            ' WHERE id = :id',
            id: result.first['id']

        @db.commit

        result.first
    end

    def update_key(qry, params = {})
        p params
        result = @db.execute qry, params
    end

    def free_key(id)
        @db.execute 'UPDATE keys SET in_use = 0, last_used = CURRENT_TIMESTAMP ' +
            'WHERE id = :id', id: id
    end

    def get_keys
        @db.execute <<-SQL
        SELECT * FROM keys
        SQL
    end

    def allocate_proxy(qry, params = {})
        @db.transaction :exclusive

        result = @db.execute qry, params
        
        if result.size == 0
            @db.rollback
            return nil
        end

        @db.execute 'UPDATE proxies SET in_use = 1 ' +
            ' WHERE id = :id',
            id: result.first['id']

        @db.commit

        result.first
    end

    def free_proxy(id)
        @db.execute 'UPDATE proxies SET in_use = 0 ' +
            'WHERE id = :id', id: id
    end

    def get_proxies
        @db.execute <<-SQL
        SELECT * FROM proxies
        SQL
    end

    def create_tables
        @db.execute <<-SQL
        CREATE TABLE IF NOT EXISTS keys (
            id INTEGER PRIMARY KEY NOT NULL,
            classic VARCHAR(26) NOT NULL UNIQUE,
            expansion VARCHAR(26) NOT NULL UNIQUE,
            username VARCHAR(15) DEFAULT '',
            banned BOOLEAN DEFAULT -1,
            muted BOOLEAN DEFAULT -1,
            in_use BOOLEAN DEFAULT 0,
            used_by_someone BOOLEAN DEFAULT 0,
            blizz_tagged BOOLEAN DEFAULT 0,
            last_used TIMESTAMP DEFAULT '1970-01-01',
            comment VARCHAR(128) DEFAULT '',
            do_not_use BOOLEAN DEFAULT 0
        )
        SQL

        @db.execute <<-SQL
        CREATE TABLE IF NOT EXISTS proxies (
            id INTEGER PRIMARY KEY NOT NULL,
            host VARCHAR NOT NULL,
            port INTEGER NOT NULL,
            username VARCHAR DEFAULT '',
            password VARCHAR DEFAULT '',
            in_use BOOLEAN DEFAULT 0,
            rd BOOLEAN DEFAULT 0,
            cant_connect BOOLEAN DEFAULT 0,
            do_not_use BOOLEAN DEFAULT 0
        )
        SQL
    end

    def add_key(classic, expansion, comment)
        name = [Faker::Name.first_name, Faker::Name.last_name].shuffle.first
        @db.execute 'INSERT INTO keys(classic, expansion, username, comment)' + 
            ' VALUES (?,?,?,?)',
            [classic, expansion, name, comment]
    end

    def add_proxy(host, port, username, password)
        @db.execute 'INSERT INTO proxies(host, port, username, password)' + 
            ' VALUES (?,?,?,?)',
            [host, port, username, password]
    end

    def execute(qry, params = {})
        @db.execute qry, params
    end
end